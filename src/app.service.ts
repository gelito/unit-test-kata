import { Injectable } from '@nestjs/common';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(User)
    protected usersRepository: Repository<User>) {
  }
  getHello(): string {
    return 'Hello World!';
  }

  async getUsers(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async getUser(user: string): Promise<User> {
    return this.usersRepository.findOne(user);
  }
}
