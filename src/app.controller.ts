import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { User } from './user.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/users')
  async getUsers(): Promise<User[]> {
    return this.appService.getUsers();
  }

  @Get('/users/:user')
  getUser(@Param() user: string): Promise<User> {
    return this.appService.getUser(user);
  }

}
