import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  fullName: string;

  @Column({ default: true })
  isActive: boolean;

  @BeforeInsert()
  protected setFullName() {
    this.fullName = `${this.firstName} ${this.lastName}`;
  }


  @BeforeUpdate()
  protected updateFullName() {
    if (!this.fullName) {
      this.setFullName();
    }
  }


}